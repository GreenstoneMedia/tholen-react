function getRoomCoordinates(
	width,
	height,
	numberOfRooms,
	boxWidth,
	boxHeight,
	hall,
	frontDoorFacing
) {
	if ( width === 0 || height === 0 || numberOfRooms === 0 ) {
		return [];
	}
	let opreatoryRoomWithWindow = 'http://134.122.28.235/img_17_april/operatoryWithWindow.jpg';
	let opreatoryRoomWithoutWindow = 'http://134.122.28.235/img_17_april/operatoryWithOutWindow.png';
	let offsetLeft = -1;
	let offsetTop = -1;
	let offsetTop__ = -1;
	let offsetBottom = 0;
	let offsetLeftExtended = 0;
	let switched = false;
	const rooms = [];
	let verticalIteration = 0;
	let showingVerticallyBoxes = false;
	for ( let i = 0; i < numberOfRooms; i++ ) {
		let rotate = 0;
		if ( frontDoorFacing === 'north' ) {
			// if front door at east, then place the room at west walls.
			offsetTop__ = height - 100;
			rotate = 180;
		}
		if ( width >= offsetLeft + boxWidth ) {
			rooms.push( {
				offsetLeft: offsetLeft,
				boxWidth: boxWidth,
				boxHeight: boxHeight,
				offsetTop: offsetTop__,
				imgUrl: opreatoryRoomWithWindow,
				rotate: rotate
			} );
			offsetLeft += boxWidth;
		} else {

			if ( !switched ) {
				//check for hall & add if
				offsetTop = offsetTop + hall;
				// offsetLeftExtended = offsetLeft - boxHeight;
				// offsetLeftExtended = offsetLeft - boxWidth;
				offsetLeftExtended = width - 100;
			}
			switched = true;
			if ( verticalIteration === 0 ) {
				offsetTop = offsetTop + boxHeight;
			} else {
				offsetTop = offsetTop + 100;
			}
			verticalIteration += 1;
			if ( height >= offsetTop + boxHeight ) {
				showingVerticallyBoxes = true;
				let image = opreatoryRoomWithWindow;
				if ( frontDoorFacing === 'east' ) {
					// if front door at east, then place the room at west walls.
					offsetLeftExtended = 7;
					// operatory room are west..
					image = opreatoryRoomWithoutWindow;
					rotate = 270;

				}
				if ( frontDoorFacing === 'north' ) {
					offsetBottom = offsetBottom + 100;
					offsetTop = null;
				}
				let manipulatedOffsetLeft = offsetLeftExtended;
				if ( frontDoorFacing === 'north' || frontDoorFacing === 'south' || frontDoorFacing === 'west' ) {
					rotate = 90;
					manipulatedOffsetLeft = manipulatedOffsetLeft - 7;
				}

				rooms.push( {
					offsetLeft: manipulatedOffsetLeft,
					boxWidth: boxWidth,
					boxHeight: boxHeight,
					offsetTop: offsetTop,
					offsetBottom: offsetBottom,
					imgUrl: image,
					rotate: rotate
				} );

			} else {
				break;
			}
		}
	}
	return { rooms: rooms, showingVerticallyBoxes: showingVerticallyBoxes };
}

export default getRoomCoordinates;
