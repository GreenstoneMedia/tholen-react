import React, {useState} from "react";
import "./Building.css";

import getRoomCoordinates from "../utils/BuildingUtil";
import Room from "./Room";
import Zones from "./Zones";
import ClearCache from "react-clear-cache";

export default function Building( props ) {
	const { width, length, numberOfRooms, layers } = props.data;
	useState( {
		lastFloor: props.lastfoor
	} );

	// const actualWidth = width * 5;
	// const actualHeight = length * 5;
	const actualWidth = width;
	const actualHeight = length;
	const rooms = getRoomCoordinates(
		actualWidth,
		actualHeight,
		numberOfRooms,
		100,
		115,
		// 50,
		// 57.5,
		40,
		props.lastfoor.frontdoor_facing
	);

	// rendering door!
	function DoorRendering( props ) {
		let percentage = props.lastfloor.door_position / 100;
		let top_axis = percentage * length;
		let left_axis = percentage * width;
		let door = null;
		switch ( props.lastfloor.frontdoor_facing ) {
			case 'north':
				door = <h2 style={{ zIndex: 100, left: left_axis + 'px', position: 'absolute', top: '-4%' }}>Door</h2>;
				break;
			case 'east':
				door = <h2 style={{ zIndex: 100, top: top_axis + 'px', position: 'absolute', right: '-4%' }}>Door</h2>;
				break;
			case 'south':
				door =
					<h2 style={{ zIndex: 100, left: left_axis + 'px', position: 'absolute', bottom: '-4%' }}>Door</h2>;
				break;
			case 'west':
				door = <h2 style={{ zIndex: 100, top: top_axis + 'px', position: 'absolute', left: '-4%' }}>Door</h2>;
				break;
			default:
				door = <span></span>;
				break
		}

		return door;
	}

	return (
		<div
			className="div-building"
			style={{ width: actualWidth + 'px', height: actualHeight + 'px', margin: '30px' }}
		>
			<DoorRendering lastfloor={props.lastfoor}/>
			{rooms.rooms.map( ( room, idx ) => (
				<Room
					key={`room_${idx}`}
					width={room.boxWidth}
					height={room.boxHeight}
					offsetLeft={room.offsetLeft}
					offsetTop={room.offsetTop}
					offsetBottom={room.offsetBottom}
					imgUrl={room.imgUrl}
					rotate={room.rotate}
				/>
			) )}

			{console.log( layers, 'layers' )}

			{layers.map( ( layer, idx ) => (
				<Zones
					key={idx}
					width={layer.width}
					height={layer.length}
					offsetLeft={layer.x}
					offsetTop={layer.y}
					bgColor={layer.bg_color}
					bgImgUrl={layer.bg_image}
					layer={layer}
					lastfloor={props.lastfoor}
					zoneName={layer.name}
					parent_id={layer.parent_id}
					childrenLayers={layer.children}
					footPrintWidth={actualWidth}
					footPrintLength={actualHeight}
					showingVerticallyRoom={rooms.showingVerticallyBoxes}
				/>
			) )}

		</div>
	);
}
