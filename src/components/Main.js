import React, {Fragment} from "react";
import Building from "./Building";
import ClearCache from "react-clear-cache";

// import Form from "./Form";

// const Resizable = require( 'react-resizable' ).Resizable; // or,
// const ResizableBox = require( 'react-resizable' ).ResizableBox;
// is using for local development
const localURL = 'http://127.0.0.1:8000/api/';
// this is using for server
const serverURL = 'http://134.122.28.235/api/';
export default class Main extends React.Component {

	state = {
		width: 500,
		length: 500,
		numberOfRooms: 13,
		layers: [],
		last_floor: [],
	};
	handleFormInput = e => {
		if ( e.target.value ) {
			this.setState( {
				[ e.target.name ]: e.target.value
			} );
		}
	};

	componentDidMount() {
		// fetch the last layer
		fetch( localURL + "last-floor" ).then( res => res.json() ).then( response => {
			this.setState( {
				length: response.length,
				width: response.width,
				numberOfRooms: response.operatory_rooms,
				last_floor: response,
			} );
			console.log( response, 'last-floor' );

		} ).catch( error => {
			console.log( error, 'Error while fetch records' );
		} );
		// call the layers
		fetch( localURL + "layers" ).then( res => res.json() ).then( response => {
			let root = [];
			response.map( ( layer, index ) => {
				layer.children = [];
				if ( layer.parent_id === 0 ) {
					root.push( layer );
				} else {
					root.filter( ( layerfilter ) => {
						if ( layerfilter.id === layer.parent_id ) {
							return layerfilter.children.push( layer );
						} else {
							return null;
						}
					} );
				}
				return root;
			} );
			this.setState( {
				layers: root
			} );
			console.log( this.state.layers, 'layers' )
		} ).catch( error => {
			console.log( error, 'Error while fetch records' );
		} );
	}

	render() {

		return (
			<Fragment>
				{/*<Form data={this.state} handleInput={this.handleFormInput}/>*/}
				<Building lastfoor={this.state.last_floor} data={this.state}/>
			</Fragment>
		);
	}
}
