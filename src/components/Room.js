import React from "react";
import "./Room.css";

export default function Room( props ) {
	const { width, height, offsetLeft, offsetTop, offsetBottom, imgUrl, rotate } = props;
	return (
		<div
			className="div-room"
			style={{
				backgroundImage: `url(${imgUrl})`,
				transform: `rotate(${rotate}deg)`,
				width,
				height,
				left: offsetLeft,
				top: offsetTop,
				bottom: offsetBottom,
				backgroundSize: 'contain',
				backgroundPosition: 'center',
				backgroundRepeat: 'no-repeat',
			}}
		/>
	);
}
