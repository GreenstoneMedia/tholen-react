import React from "react";
import "./Room.css";
import {Resizable} from 'react-resizable';
import Draggable from 'react-draggable';

let topOffsets = {};
let leftOffsets = {};
let layersWidth = {};
let layersHeight = {};
export default class Zones extends React.Component {

	state = {
		width: this.props.width,
		height: this.props.height,
		offsetLeft: this.props.offsetLeft,
		offsetTop: this.props.offsetTop,
		lastFloor: this.props.lastfloor,
		footPrintWidth: this.props.footPrintWidth,
		footPrintLength: this.props.footPrintLength,
		offsetBottom: null,
		offsetTopArray: {},
		childrenLayers: this.props.childrenLayers
	};

	onResize = ( event, { element, size, handle } ) => {
		this.setState( { width: size.width, height: size.height } );
	};

	settingLayerSize = () => {
		let width_ = this.state.width;
		let height_ = this.state.height;
		// subtracting from east
		let subtract_ops_room_width = 0;
		let offsetTop = this.state.offsetTop;
		let offsetLeft = this.state.offsetLeft;
		let offsetBottom = null;

		let door_percentage = this.state.lastFloor.door_position / 100;
		let door_top_axis = door_percentage * this.state.footPrintLength;
		let door_left_axis = door_percentage * this.state.footPrintWidth;

		if ( this.props.showingVerticallyRoom ) {
			if ( this.state.lastFloor.frontdoor_facing === 'east' ) {
				// if front door at west wall , then place the room at east walls and substract  from east
				subtract_ops_room_width = 115;
				offsetLeft = offsetLeft + 115;
			}
			if ( this.state.lastFloor.frontdoor_facing === 'north' ) {
				subtract_ops_room_width = 115;
			} else if ( this.state.lastFloor.frontdoor_facing === 'west' || this.state.lastFloor.frontdoor_facing === 'south' ) {
				// if front door at east wall, then place the room at west walls and substract from west
				subtract_ops_room_width = 115;
			}
		}
		if ( this.state.lastFloor.frontdoor_facing === 'north' ) {
			// if door at north side then zones will touch to the wall.
			offsetTop = offsetTop + 155;
		}
		if ( this.props.layer.type === 'p_zone' ) {
			// private zone
			height_ = this.props.footPrintLength - 155;
			width_ = (this.props.footPrintWidth - subtract_ops_room_width) / 2;

			if ( this.state.lastFloor.frontdoor_facing === 'north' ) {
				// offsetTop = (this.props.footPrintLength - 155) / (2);
				offsetTop = 0;
				// offsetLeft = (this.props.footPrintWidth - subtract_ops_room_width) / 2;
				if ( door_left_axis + 5 < width_ ) {
					offsetLeft = (this.props.footPrintWidth - subtract_ops_room_width) / 2;
				}
			}
			// offset south
			if ( this.state.lastFloor.frontdoor_facing === 'south' ) {
				if ( door_left_axis + 5 < width_ ) {
					offsetLeft = (this.props.footPrintWidth - subtract_ops_room_width) / 2;
				}
			}
			if ( this.state.lastFloor.frontdoor_facing === 'west' ) {
				offsetLeft = width_;
			}
			topOffsets.private_zone = offsetTop;
			leftOffsets.private_zone = offsetLeft;
			layersHeight.private_zone = height_;
			layersWidth.private_zone = width_;

			this.setState( {
				width: width_,
				height: height_,
				offsetTop: offsetTop,
				offsetLeft: offsetLeft,
				offsetBottom: offsetBottom,
			} );

		} else if ( this.props.layer.type === 'pu_zone' ) {
			// public zone
			height_ = (this.props.footPrintLength - 155) / (2);
			width_ = (this.props.footPrintWidth - subtract_ops_room_width) / 2;
			// calculating public zone x-axis, public zone left will be equal to the Private zone width
			offsetLeft = width_;
			// calculating public zone y-axis, public zone top will be equal to Operatory length 115 + Hallway Length 40 and Clinical zone length
			if ( this.state.lastFloor.frontdoor_facing === 'north' ) {
				// offsetTop = (this.props.footPrintLength - 155) / (2);
				offsetTop = 0;

				if ( door_left_axis + 5 < (this.props.footPrintWidth - subtract_ops_room_width) / 2 ) {
					offsetLeft = 0;
				}

			} else {
				offsetTop = (155) + ((this.props.footPrintLength - 155) / (2));
			}
			// offset south
			if ( this.state.lastFloor.frontdoor_facing === 'south' ) {
				if ( door_left_axis + 5 < width_ ) {
					offsetLeft = 0;
				}
			}
			if ( this.state.lastFloor.frontdoor_facing === 'west' ) {
				offsetLeft = 0;
			}
			if ( this.state.lastFloor.frontdoor_facing === 'west' || this.state.lastFloor.frontdoor_facing === 'east' ) {
				// if door is in the Clinical zone area then switch clinical zone to the bottom of surface.
				if ( door_top_axis + 30 >= (155) + ((this.props.footPrintLength - 155) / (2)) ) {
				} else {
					offsetTop = 155;
				}
			}

			if ( this.state.lastFloor.frontdoor_facing === 'east' ) {
				// offsetLeft = layersWidth.private_zone + offsetLeft.private_zone;
			}

			topOffsets.public_zone = offsetTop;
			leftOffsets.public_zone = offsetLeft;
			layersHeight.public_zone = height_;
			layersWidth.public_zone = width_;

			this.setState( {
				width: width_,
				height: height_,
				offsetTop: offsetTop,
				offsetLeft: offsetLeft,
				offsetBottom: offsetBottom,
			} );

		} else if ( this.props.layer.type === 'c_zone' ) {
			// clinical zone
			height_ = (this.props.footPrintLength - 155) / (2);
			width_ = ((this.props.footPrintWidth - subtract_ops_room_width) / 2) - 40;

			// calculating clinical zone x-axis, clinical zone left will be equal to the Private zone width + 40px (hallway)
			offsetLeft = ((this.props.footPrintWidth - subtract_ops_room_width) / 2) + (40);
			// offsetLeft =  offsetLeft.private_zone+layersWidth.private_zone + (40);

			if ( this.state.lastFloor.frontdoor_facing === 'north' ) {
				offsetTop = (this.props.footPrintLength - 155) / (2);
				if ( door_left_axis + 5 < (this.props.footPrintWidth - subtract_ops_room_width) / 2 ) {
					offsetLeft = 0;
				}
			}
			// offset south
			if ( this.state.lastFloor.frontdoor_facing === 'south' ) {
				if ( door_left_axis + 5 < width_ ) {
					offsetLeft = 0;
				}
			}
			if ( this.state.lastFloor.frontdoor_facing === 'west' ) {
				offsetLeft = 0;
			}

			if ( this.state.lastFloor.frontdoor_facing === 'west' || this.state.lastFloor.frontdoor_facing === 'east' ) {
				// if door is in the Clinical zone area then switch clinical zone to the bottom of surface.
				if ( door_top_axis + 30 >= (155) + ((this.props.footPrintLength - 155) / (2)) ) {
				} else {
					offsetTop = 155 + (this.props.footPrintLength - 155) / (2);
				}

			}
			topOffsets.clinicial_zone = offsetTop;
			leftOffsets.clinicial_zone = offsetLeft;
			layersHeight.clinicial_zone = height_;
			layersWidth.clinicial_zone = width_;

			this.setState( {
				width: width_,
				height: height_,
				offsetTop: offsetTop,
				offsetLeft: offsetLeft,
				offsetBottom: offsetBottom,
			} );

		} else if ( this.props.layer.type === 'pu_zone_child' ) {
			let publicZoneWidth = (this.props.footPrintWidth - subtract_ops_room_width) / 2;
			let clinicalZoneHeight = (this.props.footPrintLength - 155) / (2);

			switch ( this.props.layer.name ) {
				case 'Patient Toilet 9x9':
					offsetLeft = publicZoneWidth;
					offsetTop = clinicalZoneHeight + 155;
					break;
				case 'Semiprivate Arrangements Desk 2.5x2.5':
					offsetLeft = publicZoneWidth;
					offsetTop = clinicalZoneHeight + 155 + 90;
					break;
				case 'Reception & Appointment 12x6':
					offsetLeft = publicZoneWidth;
					break;
				default:
					break;
			}

			// if inner layers length going to increase from footprint length then decrease to the footprint length
			if ( (Math.abs( offsetTop ) + this.props.layer.length) > this.props.footPrintLength ) {
				offsetTop = this.props.footPrintLength - this.props.layer.length;
			}

			this.setState( {
				width: width_,
				height: height_,
				offsetTop: offsetTop,
				offsetLeft: offsetLeft,
				offsetBottom: offsetBottom,
			} );

		} else if ( this.props.layer.type === 'c_zone_child' ) {

			// clinical zone inner layers
			// === dealing with the x and y-axis, top of the layer....
			let clinicalZoneWidth = ((this.props.footPrintWidth - subtract_ops_room_width) / 2) + (40);
			let clinicalZoneOffsetLeft = ((this.props.footPrintWidth - subtract_ops_room_width) / 2) + (40);
			let clinicalZoneHeight = (this.props.footPrintLength - 155) / (2);
			let privateZoneWidth = (this.props.footPrintWidth - subtract_ops_room_width) / 2;

			switch ( this.props.layer.name ) {
				case 'Lab':
					offsetLeft = clinicalZoneWidth;
					break;
				case 'L Shaped Sterilization':
					offsetLeft = (clinicalZoneOffsetLeft + clinicalZoneWidth) - (this.props.layer.width * 2);
					break;
				default:
					break;
			}

			// if inner layers length going to increase from footprint length then decrease to the footprint length
			if ( (Math.abs( offsetTop ) + this.props.layer.length) > this.props.footPrintLength ) {
				offsetTop = this.props.footPrintLength - this.props.layer.length;
			}

			this.setState( {
				width: width_,
				height: height_,
				offsetTop: offsetTop,
				offsetLeft: offsetLeft,
				offsetBottom: offsetBottom,
			} );

		} else if ( this.props.layer.type === 'p_zone_child' ) {
			// Private zones inner layers.

			// === dealing with the x and y-axis, top of the layer....
			// if private zone child top is going beyond the footprint length, then take it to decrease
			if ( Math.abs( this.props.layer.y ) > this.props.footPrintLength ) {
				offsetTop = this.props.footPrintLength - this.props.layer.length;
			}

			// if inner layers length going to increase from footprint length then decrease to the footprint length
			if ( (Math.abs( this.props.layer.y ) + this.props.layer.length) > this.props.footPrintLength ) {
				offsetTop = this.props.footPrintLength - this.props.layer.length;
			}

			let private_zone_width = (this.props.footPrintWidth - subtract_ops_room_width) / 2;
			switch ( this.props.layer.name ) {
				case 'Mechanical Room 10x10':
				case 'Trunk Corridor X-ray Alcove 5x5':
				case 'Consult Room':
					offsetLeft = private_zone_width - this.props.layer.width;
					break;
				case 'Satellite Sterilization':
					if ( this.props.layer.x > private_zone_width ) {
						offsetLeft = private_zone_width - this.props.layer.width - 222;
					}
					break;
				case 'Trunk Corridor Bulk Storage Space':
					console.log( this.props.layer );
					offsetLeft = private_zone_width - this.props.layer.width;
					break;
				default:
					break;
			}

			this.setState( {
				width: width_,
				height: height_,
				offsetTop: offsetTop,
				offsetLeft: offsetLeft,
				offsetBottom: offsetBottom,
			} );

		} else if ( this.props.layer.name === 'Reception & Appointment Desk' ) {
			// offsetTop = (layersHeight.clinicial_zone + layersHeight.public_zone + 155) - this.props.layer.length;
			// // offsetTop = door_top_axis;
			// if ( this.state.lastFloor.frontdoor_facing === 'west' ) {
			// 	offsetLeft = layersWidth.clinicial_zone - this.props.layer.width + 40;
			// }
			// if ( this.state.lastFloor.frontdoor_facing === 'east' ) {
			// 	offsetLeft = leftOffsets.clinicial_zone - 40;
			// }
			// if ( this.state.lastFloor.frontdoor_facing === 'north' ) {
			// 	offsetLeft = layersWidth.private_zone;
			// 	if ( door_left_axis + 5 < (this.props.footPrintWidth - subtract_ops_room_width) / 2 ) {
			// 		offsetLeft = layersWidth.clinicial_zone;
			// 	}
			// }
			//
			// if ( this.state.lastFloor.frontdoor_facing === 'west' || this.state.lastFloor.frontdoor_facing === 'east' ) {
			// 	// if door is in the Clinical zone area then switch clinical zone to the bottom of surface.
			// 	if ( door_top_axis + 30 >= (155) + ((this.props.footPrintLength - 155) / (2)) ) {
			// 	} else {
			// 		// offsetTop = (155 + (this.props.footPrintLength - 155) / (2) );
			// 		offsetTop = (layersHeight.public_zone + 155) - this.props.layer.length;
			// 	}
			// }
			//
			// offsetTop = door_top_axis;

		} else {
			// other layers size will not be greater than the footprint size
			if ( this.props.layer.length > this.props.footPrintLength ) {
				height_ = parseInt( this.props.footPrintLength );
			}
			if ( this.props.layer.width > this.props.footPrintWidth ) {
				width_ = parseInt( this.props.footPrintWidth );
			}

			this.setState( {
				width: width_,
				height: height_,
				offsetTop: offsetTop,
				offsetLeft: offsetLeft,
				offsetBottom: offsetBottom,
			} );

		}

	};
	settingChildrenLayerSizes = () => {

		let door_percentage = this.state.lastFloor.door_position / 100;
		let door_top_axis = door_percentage * this.state.footPrintLength;
		let door_left_axis = door_percentage * this.state.footPrintWidth;

		let childrenLayersAdjustment = this.state.childrenLayers.map( ( layer ) => {
			if ( layer.type === 'p_zone_right_wall_child' ) {
				layer.x = layersWidth.private_zone - layer.width;
			}

			if ( layer.name === 'Reception & Appointment Desk' ) {
				if ( this.state.lastFloor.frontdoor_facing === 'east' ) {
					layer.x = layersWidth.public_zone - layer.width;
				}

				if ( this.state.lastFloor.frontdoor_facing === 'west' || this.state.lastFloor.frontdoor_facing === 'east' ) {
					if ( topOffsets.public_zone + (0.50 * layersHeight.public_zone) < door_top_axis ) {
						layer.y = layersHeight.public_zone - layer.length;
					} else {
						console.log( 'Place at the top..' );
					}
				}

				if ( this.state.lastFloor.frontdoor_facing === 'north' || this.state.lastFloor.frontdoor_facing === 'south' ) {
					if ( leftOffsets.public_zone + (0.50 * layersWidth.public_zone) < door_left_axis ) {
						layer.x = layersWidth.public_zone - layer.width;
					}
				}

				if ( this.state.lastFloor.frontdoor_facing === 'north' ) {
					layer.y = 0;
				}

				if ( this.state.lastFloor.frontdoor_facing === 'south' ) {
					layer.y = layersHeight.public_zone - layer.length;
				}

			}
			return layer;
		} )
	}

	componentDidMount = () => {
		// this.settingLayerSize();
		// this.settingChildrenLayerSizes();
	};

	render() {
		const { bgColor, bgImgUrl, zoneName } = this.props;
		let { width, height } = this.state;
		const dragHandlers = { onStart: this.onStart, onStop: this.onStop };

		return (
			// <Draggable handle="strong" {...dragHandlers}>
			// 	<Resizable
			// 		className="box"
			// 		height={height} width={width}
			// 		onResize={this.onResize}>
			//
			//
			// 	</Resizable>
			// </Draggable>

			<div
				className="div-room  box no-cursor"
				style={{
					width: width,
					height: height,
					left: this.state.offsetLeft,
					top: Math.abs( this.state.offsetTop ),
					bottom: this.state.offsetBottom,
					background: `${bgColor}`,
					backgroundImage: `url(${bgImgUrl})`,
					wordWrap: "break-word",
					// zIndex: this.props.layer.layer_priority,
					display: `${this.props.layer.hide === 1 ? 'none' : 'block'}`,
					backgroundSize: 'contain',
					backgroundPosition: 'center',
					backgroundRepeat: 'no-repeat',
				}}
			>
				{/*<span className="tooltiptext--">*/}
				{/*  {zoneName}*/}
				{/*</span>*/}
				{/*<strong className="cursor">*/}
				{/*	<div style={{ 'cursor': 'pointer' }}>D</div>*/}
				{/*</strong>*/}

				{this.state.childrenLayers.map( ( childrenLayer, childrenLayerIndex ) => {
					return (
						<div className='inner-layers tooltip' key={childrenLayerIndex}
						     style={{
							     position: "absolute",
							     width: childrenLayer.width,
							     height: childrenLayer.length,
							     left: childrenLayer.x,
							     top: Math.abs( childrenLayer.y ),
							     background: `${childrenLayer.bg_color}`,
							     backgroundImage: `url(${childrenLayer.bg_image})`,
							     wordWrap: "break-word",
							     // zIndex: childrenLayer.layer_priority,
							     display: `${childrenLayer.hide === 1 ? 'none' : 'block'}`,
							     backgroundSize: 'contain',
							     backgroundPosition: 'center',
							     backgroundRepeat: 'no-repeat',
						     }}
						>

							<span className="tooltiptext">
								{childrenLayer.name} {childrenLayer.width} x {childrenLayer.length}
							</span>
						</div>
					);
				} )}

			</div>

		);
	}

}
